# FlipBookPDF

### Build

```bash
$ yarn install
$ yarn build
```

### Installation & Usage

```html
<!DOCTYPE html>
<html>
    <head>
        <mate charest="utf-8" />
        <title>Hello world!</title>
		<link rel="stylesheet" type="text/css" href="flipbookpdf/dist/index.css" />
    </head>
    <body>
		<flip-book pdf-url="./documento.pdf"></flip-book>
        <script src="flipbookpdf/dist/index.js"></script>
    </body>
</html>
```