import Vue from 'vue';
import Viewer from './viewer.vue';
//import pdfjsLib from './lib/pdf'
import pdfjsLib from 'pdfjs-dist/build/pdf';
import pdfjsWorker from 'pdfjs-dist/build/pdf.worker.entry';

import '@webcomponents/custom-elements';

pdfjsLib.GlobalWorkerOptions.workerSrc = pdfjsWorker;

const getPdfPages = async url => {  
    let pdf = await pdfjsLib.getDocument(url).promise;
    
    let pages = await Promise.all(new Array(pdf.numPages).fill(0).map((_,i) => pdf.getPage(i+1)));
    
    pages = await Promise.all(pages.map(async page => {
        let viewport = page.getViewport({scale: 1});
        let desiredWidth = (window.innerWidth * 0.9) / 2;
        let desiredHeight = window.innerHeight * 0.9 - 60;
        
        var scaleWidth = desiredWidth / viewport.width;
        var scaleHeight = desiredHeight / viewport.height;

        let scale = Math.min(scaleWidth, scaleHeight) * 5;

        viewport = page.getViewport({scale});
        let canvas = document.createElement('canvas');
        

        canvas.width = viewport.width;
        canvas.height = viewport.height;
        let context = canvas.getContext('2d');
        let renderContext = {
            canvasContext: context,
            viewport
        };
        
        await page.render(renderContext).promise;
        
        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                resolve(window.URL.createObjectURL(blob));
            });
        });
    }));
    return pages;
};

class FlipBook extends HTMLElement {
    constructor() {
        super();
        let url = this.getAttribute('pdf-url');                
        let div = document.createElement('div');
        this.appendChild(div);
        (async () => {
            let pages = await getPdfPages(url);
            new Vue({
                el: this,
                render: h => h(Viewer, { props: {
                    pages
                }})
            });
        })();
    }
}

customElements.define('flip-book', FlipBook);